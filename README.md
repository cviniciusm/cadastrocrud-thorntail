# Cadastro CRUD (thorntail version)

[![Known Vulnerabilities](https://snyk.io/test/github/cassiusvm/cadastrocrud-thorntail/badge.svg)](https://snyk.io/test/github/cassiusvm/cadastrocrud-thorntail)

Sample application that does a registration (performs CRUD operations)

This project is a thorntail version of the project [cadastrocrud](https://github.com/cassiusvm/cadastrocrud) with mininum changes.

## Tecnologies/frameworks/tools

* Hibernate 5.3
* IDE Eclipse 2019-16
* JDK 11
* JPA 2.2
* JSF 2.3
* MariaDB 10.1
* Maven Wrapper 3.6
* Omnifaces 3.3
* Primefaces 7.0
* Project Lombok 1.18
* Thorntail 2.4