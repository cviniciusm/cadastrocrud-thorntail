package br.eti.cvm.cadastrocrud.bean.crud;

import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.annotation.ManagedProperty;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

import br.eti.cvm.cadastrocrud.model.Person;
import br.eti.cvm.cadastrocrud.service.LogService;
import br.eti.cvm.cadastrocrud.service.PersonService;
import lombok.Getter;
import lombok.Setter;

@Named
@RequestScoped
public class CreateBean {

	@Getter
	@Setter
	private Person person;

	@Inject
	private PersonService personService;

	@Inject
	private Flash flash;

	@Inject
	private FacesContext facesContext;

	@Inject
	@ManagedProperty("#{label}")
	private ResourceBundle label;

	@Inject
	private LogService logService;

	@PostConstruct
	private void init() {
		logService.logInfo("método init foi chamado");

		person = new Person();
	}

	public String saveAction() {
		logService.logInfo("método salvar foi chamado");

		personService.create(person);

		flash.setKeepMessages(true);
		facesContext.addMessage(null, new FacesMessage(label.getString("recordcreatedsuccessfully")));

		return "retrieve?faces-redirect=true";
	}

	public void validateEmailDuplicated(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		logService.logInfo("método validateEmailRepetido foi chamado");

		Person person = personService.findByEmail((String) value);
		if (person != null) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
					label.getString("emailcannotbeduplicated"), label.getString("emailcannotbeduplicated")));
		}

		return;
	}

}