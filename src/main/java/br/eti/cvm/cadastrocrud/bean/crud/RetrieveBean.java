package br.eti.cvm.cadastrocrud.bean.crud;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.annotation.ManagedProperty;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.eti.cvm.cadastrocrud.model.Person;
import br.eti.cvm.cadastrocrud.service.LogService;
import br.eti.cvm.cadastrocrud.service.PersonService;
import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class RetrieveBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private String firstName;

	@Getter
	@Setter
	private String lastName;

	@Getter
	@Setter
	private String email;

	@Getter
	@Setter
	private Person personSelected;

	@Getter
	private LazyDataModel<Person> tablePaginated;

	@Inject
	PersonService personService;

	@Inject
	private Flash flash;

	@Inject
	private FacesContext facesContext;

	@Inject
	@ManagedProperty("#{label}")
	private ResourceBundle label;
	
	@Inject
	LogService logService;

	@PostConstruct
	private void init() {
		logService.logInfo("método init foi chamado");

		firstName = new String();
		lastName = new String();
		email = new String();

		tablePaginated = new LazyDataModel<Person>() {
			private static final long serialVersionUID = 1L;

			private List<Person> datasource = new ArrayList<Person>();

			@Override
			public Person getRowData(String rowKey) {
				logService.logInfo("rowKey = " + rowKey);

				Long id = Long.valueOf(rowKey);

				for (Person person : datasource) {
					if (id.equals(person.getId()))
						return person;
				}

				return null;
			}

			@Override
			public List<Person> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				datasource = personService.searchByFullNameEmail(firstName, lastName, email, first, pageSize);

				tablePaginated.setRowCount(personService.count().intValue());

				return datasource;
			}
		};
	}

	public String createAction() {
		logService.logInfo("método createAction foi chamado");

		flash.putNow("person", personSelected);

		return "create?faces-redirect=true";
	}

	public String updateAction() {
		logService.logInfo("método updateAction foi chamado");

		if (personSelected == null) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					label.getString("norecordselected"), label.getString("norecordselected")));
			return null;
		}

		flash.putNow("person", personSelected);

		return "update?faces-redirect=true";
	}

	public String deleteAction() {
		logService.logInfo("método deleteAction foi chamado");

		if (personSelected == null) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					label.getString("norecordselected"), label.getString("norecordselected")));
			return null;
		}

		flash.putNow("person", personSelected);

		return "delete?faces-redirect=true";
	}

	public void cleanSearch(ActionEvent actionEvent) {
		firstName = new String();
		lastName = new String();
		email = new String();
	}

}