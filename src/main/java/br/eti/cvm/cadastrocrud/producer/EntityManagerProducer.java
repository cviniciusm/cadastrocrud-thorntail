package br.eti.cvm.cadastrocrud.producer;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class EntityManagerProducer {
	
	@PersistenceContext(unitName = "cadastrocrud-persistence-unit")
	private EntityManager entityManager;
	
	@EntityMangerQualifier
	@Produces
	@SessionScoped
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
}
