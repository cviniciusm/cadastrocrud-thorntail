package br.eti.cvm.cadastrocrud.service;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import br.eti.cvm.cadastrocrud.bean.ProjectStageBean;
import br.eti.cvm.cadastrocrud.model.Person;
import br.eti.cvm.cadastrocrud.repository.PersonRepository;
import lombok.extern.jbosslog.JBossLog;

@JBossLog
@SessionScoped
public class PersonService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	PersonRepository personRepo;

	@Inject
	private ProjectStageBean projectStage;

	public void create(Person entity) {
		personRepo.create(entity);
	}

	public Person retrieve(Long id) {
		return personRepo.retrieve(id);
	}

	public Person findByEmail(String email) {
		return personRepo.findByEmail(email);
	}

	public List<Person> searchByFullNameEmail(String name, String lastName, String email, Integer startPosition,
			Integer maxResult) {
		return personRepo.searchByFullNameEmail(name, lastName, email, startPosition, maxResult);
	}

	public List<Person> listAll(Integer startPosition, Integer maxResult) {
		return personRepo.retrieveAll();
	}

	public Long count() {
		return personRepo.count();
	}

	public Person update(Person entity) {
		return personRepo.update(entity);
	}

	public void delete(Person entity) {
		personRepo.delete(entity);
	}

	public void logInfo(String info) {
		if (projectStage.isNotProduction())
			log.info(info);
	}

}
